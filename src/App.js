import logo from "./logo.svg";
import "./styles/main.scss";
import "./App.scss";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./MovieTheater/Page/HomePage/HomePage";
import LoginPage from "./MovieTheater/Page/LoginPage/LoginPage";
import Layout from "./MovieTheater/Layout/Layout";
import DetailPage from "./MovieTheater/Page/DetailPage/DetailPage";
import Spinner from "./MovieTheater/Component/Spinner";
import RegistrationPage from "./MovieTheater/Page/RegistrationPage/RegistrationPage";
import CheckOut from "./MovieTheater/Page/CheckOutPage/CheckOut";
import SearchPage from "./MovieTheater/Page/SearchPage/SearchPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Spinner />
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route
            path="/sign-up"
            element={<Layout Component={RegistrationPage} />}
          />
          <Route
            path="/check-out/:id"
            element={<Layout Component={CheckOut} />}
          />
          <Route path="/search" element={<SearchPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
