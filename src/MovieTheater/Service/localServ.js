export const localServ = {
    set : (userInfo)=>{
        let dataJSON = JSON.stringify(userInfo)
        localStorage.setItem('USER_LOGIN', dataJSON)
    },
    get : () => {
     let dataJSOn = localStorage.getItem('USER_LOGIN')
     return JSON.parse(dataJSOn)
    },
    remove : () => {
     localStorage.removeItem('USER_LOGIN')
    }
}