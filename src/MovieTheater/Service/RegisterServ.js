import axios from "axios"
import { BASE_URL, configHeaders } from "./Config"

export const registerServ = {
    postRegister : (valueDangKy) => {
    return axios.post(`${BASE_URL}/api/QuanLyNguoiDung/DangKy`, valueDangKy ,{headers : configHeaders()})
    }
}