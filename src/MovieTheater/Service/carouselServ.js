import axios from "axios"
import { BASE_URL, configHeaders } from "./Config"

export const carouselServ = {
    getCarousel : () => {
     return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`, {headers : configHeaders()})
    }
}