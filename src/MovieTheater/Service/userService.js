import axios from "axios"
import { BASE_URL, configHeaders, userHeaders } from "./Config"

export const userServ = {
    postLogin : (data) => {
    return axios.post(`${BASE_URL}/api/QuanLyNguoiDung/DangNhap`, data, {headers : configHeaders()})
    },
    postThongTinUser : (data,userToken) => {
     return axios.post(`${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,data ,{headers : userHeaders(userToken)})
    }
}