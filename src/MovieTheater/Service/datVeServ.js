import axios from "axios"
import { BASE_URL, configHeaders, userHeaders } from "./Config"

export const datVeServ = {
    getDatVe : (maLichChieu) => {
    return axios.get(`${BASE_URL}/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,  {headers : configHeaders()})
    },
    postDatVe : (data,tokenUser) => {
     return axios.post(`${BASE_URL}/api/QuanLyDatVe/DatVe`, data , {headers : userHeaders(tokenUser)})
    }
}