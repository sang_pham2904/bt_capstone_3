import axios from "axios"
import { BASE_URL, configHeaders } from "./Config"

export const movieServ = {
    getMovieList : () => {
     return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`,{headers : configHeaders()})
    },
    getMovieDetail : (id) => {
     return axios.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,{headers : configHeaders()})
    },
    getMovieByTheater : () => {
     return axios.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07`, {headers : configHeaders()})
    },
    
}