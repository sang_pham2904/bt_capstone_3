import { REGISTER } from "../constant/registerConstant";

export const postRegisterAction = (value) => ({
  type: REGISTER,
  payload: value
})
