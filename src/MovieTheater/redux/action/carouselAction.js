import { carouselServ } from "../../Service/carouselServ";
import { SET_CAROUSEL } from "../constant/carouselConstant";
import { BAT, TAT } from "../constant/spinnerConstant";

export const getCarouselAction = () => {
  return (dispatch) => {
    dispatch({
      type: BAT,
    });
    carouselServ
      .getCarousel()
      .then((res) => {
        //  console.log(res);
        dispatch({
          type: SET_CAROUSEL,
          payload: res.data.content,
        });
        dispatch({
          type: TAT,
        });
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: TAT,
        });
      });
  };
};
