import { userServ } from "../../Service/userService";
import { BAT, TAT } from "../constant/spinnerConstant";
import { LOGIN } from "../constant/userConstant";

export const userAction = (value) => ({
  type: LOGIN,
  payload : value
})

export const userActionThunk = (value,onSuccess,onErr) => {
 return (dispatch) => {
  dispatch({
    type : BAT
  })
  userServ.postLogin(value)
        .then((res) => {
          console.log(res);
          onSuccess(res)
          dispatch({
            type : LOGIN,
            payload : res.data.content
          })
          dispatch({
            type: TAT
          })
        })
        .catch((err) => {
          dispatch({
            type: TAT
          })
          onErr(err.response.data.content)
         console.log(err);
        });
 }
}