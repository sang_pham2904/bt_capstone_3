import { ThongTinPhongVe } from "../../_core/models/ThongTinPhongVe"
import { CHON_GHE, LAYTTPHONFVE } from "../constant/datVeConstant"

const initialState = {
    chiTietPhongVe : new ThongTinPhongVe,
    danhSachGheDaChon : [],
    // danhSachGheKhachDat : [{maGhe: 55257},{maGhe : 55258}]
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

    case LAYTTPHONFVE:
        return {...state, chiTietPhongVe: payload}
    case CHON_GHE:{
      let cloneDanhSachGheDaChon = [...state.danhSachGheDaChon]
      let indexGheDangChon =  cloneDanhSachGheDaChon.findIndex((item) => {
        return item.maGhe === payload.maGhe
        })
        if(indexGheDangChon === -1){
          cloneDanhSachGheDaChon.push(payload)
        } else{
        cloneDanhSachGheDaChon.splice(indexGheDangChon,1)
        }
      return {...state, danhSachGheDaChon: cloneDanhSachGheDaChon}
    }
  default:
    return state
  }
}
