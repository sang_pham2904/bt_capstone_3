import { localServ } from "../../Service/localServ"
import { userInfo } from "../../_core/models/userInfoModel"
import { REGISTER } from "../constant/registerConstant"
import { LOGIN } from "../constant/userConstant"

const initialState = {
    userInfo : localServ.get()
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

  case LOGIN:
    return { ...state, userInfo: payload  }

  case REGISTER:
    return {...state, userInfo: payload}
  default:
    return state
  }
}
