import { BAT, TAT } from "../constant/spinnerConstant"

const initialState = {
    isLoading : false
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

  case BAT:
    return { ...state, isLoading : true }
  case TAT:
    return { ...state, isLoading : false }

  default:
    return state
  }
}
