import { SET_CAROUSEL } from "../constant/carouselConstant"

const initialState = {
   arrImg:  []
  }

export default (state = initialState, { type, payload }) => {
  switch (type) {

  case SET_CAROUSEL: 
    return { ...state,arrImg: payload}

  default:
    return state
  }
}
