import { combineReducers } from "redux";
import userReducer from "./userReducer";
import carouselReducer from "./carouselReducer";
import spinnerReducer from "./spinnerReducer";
import datVeReducer from "./datVeReducer";

export const rootReducer = combineReducers({
    userReducer,carouselReducer,spinnerReducer,datVeReducer
})