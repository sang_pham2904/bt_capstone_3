import React from "react";
import Header from "../Component/Header";
import { useLocation } from "react-router-dom";
import Footer from "../Page/Footer/Footer";

export default function Layout({ Component }) {
  const location = useLocation();
  const isHomePage = location.pathname === "/";
  const isLogin = location.pathname === "/login"
  const isSign = location.pathname === "/sign-up"
  let footer = () => {
    if(isLogin || isSign){
      return ;
    }else{
      return  <Footer/>
    }
  }
  return (
    <div className="main">
      {isHomePage ? <Header isFixed={true} /> : <Header isFixed={false} />}

      <Component />
      {footer()}
      {/* <Footer /> */}
    </div>
  );
}
