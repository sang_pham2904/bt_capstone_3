import React, { useEffect, useState } from "react";
import "../../../styles/pages/carousel.scss";
import { useDispatch, useSelector } from "react-redux";
import { Carousel } from "antd";
import axios from "axios";
import { carouselServ } from "../../Service/carouselServ";
import { SET_CAROUSEL } from "../../redux/constant/carouselConstant";
import { getCarouselAction } from "../../redux/action/carouselAction";

import ReactPlayer from "react-player";
import { useNavigate, useParams } from "react-router-dom";
import { ArrowRightOutlined } from "@ant-design/icons";
import { HashLink } from "react-router-hash-link";

export default function HomeCarousel() {
  let navigate = useNavigate();
  const { arrImg } = useSelector((state) => {
    return state.carouselReducer;
  });
  // console.log(arrImg);
  let dispatch = useDispatch();
  useEffect(() => {
    // carouselServ.getCarousel().then((res) => {
    //         console.log(res.data.content);
    //         dispatch({
    //             type : SET_CAROUSEL,
    //             payload : res.data.content
    //         })
    //       })
    //       .catch((err) => {
    //        console.log(err);
    //       });
    dispatch(getCarouselAction());
  }, []);

  let renderCarousel = () => {
    return arrImg.map((item, index) => {
      return (
        <div className="carousel-img" key={index}>
          <img
            // className="w-full"
            style={{ width: "100vw" }}
            src={item.hinhAnh}
            alt=""
          />

          {/* <ReactPlayer url={item.hinhAnh} width='100%' playing={true}/> */}
          {/* <iframe src='https://www.youtube.com/embed/E7wJTI-1dvQ'
        frameborder='0'
        allow='autoplay; encrypted-media'
        allowfullscreen
        title='video'
/> */}
        </div>
      );
    });
  };

  //smooth scroll
  function smoothScroll(event, targetId) {
    event.preventDefault();
    const targetElement = document.getElementById(targetId);
    const targetPosition = targetElement.getBoundingClientRect().top;
    window.scrollBy({
      top: targetPosition,
      left: 0,
      behavior: "smooth",
    });
  }

  let params = useParams();

  return (
    <div className="carousel relative">
      <Carousel dotPosition="bottom" effect="fade" autoplay>
        {renderCarousel()}
      </Carousel>
      <div className="container carousel-content space-x-5">
        <HashLink to="/#listMovie" smooth>
          Lịch chiếu
        </HashLink>
        <HashLink to="/#rapPhim" smooth>
          Cụm rạp
        </HashLink>
      </div>
    </div>
  );
}
