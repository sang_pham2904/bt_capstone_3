import React, { useEffect } from 'react'
import ListMovie from './ListMovie'
import TabMovie from './TabMovie/TabMovie'
import HomeCarousel from './Carousel'
export default function HomePage() {
  useEffect(() => {
    window.scrollTo(0,0)
   },[])
  return (
    <div className='space-y-10'>
      <HomeCarousel/>
      <ListMovie/>
      <TabMovie/>
    </div>
  )
}
