import React, { useEffect, useState } from "react";
import "../../../styles/pages/listMovie.scss";
import { movieServ } from "../../Service/movieServ";
import CardMovie from "./CardMovie";
import { useDispatch } from "react-redux";
import {
  batSpinnerAction,
  tatSpinnerAction,
} from "../../redux/action/spinnerAction";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

export default function ListMovie() {
  const [movieList, setMovieList] = useState([]);
  const [movieListDefault, setMovieListDefault] = useState([]);
  // console.log("movieList: ", movieList);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(batSpinnerAction());
    movieServ
      .getMovieList()
      .then((res) => {
        dispatch(tatSpinnerAction());
        setMovieList(
          res.data.content.filter((item) => {
            return item.dangChieu == true;
          })
        );
        setMovieListDefault(res.data.content);
      })
      .catch((err) => {
        dispatch(tatSpinnerAction());
        console.log(err);
      });
  }, []);

  let settings = {
    autoplay: true,
    className: "center",
    // centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500,
    // rows: 1,
    slidesToScroll: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesPerRow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesPerRow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 425,
        settings: {
          slidesToShow: 1,
          slidesPerRow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };

  let activeClassDC =
    movieList[0]?.dangChieu == true ? "active_button" : "none_active_button";
  let activeClassSC =
    movieList[0]?.sapChieu == true ? "active_button" : "none_active_button";

  let renderPhimDangChieu = () => {
    setMovieList(
      movieListDefault.filter((item) => {
        return item.dangChieu == true;
      })
    );
  };
  let renderPhimSapChieu = () => {
    setMovieList(
      movieListDefault.filter((item) => {
        return item.sapChieu == true;
      })
    );
  };

  return (
    <div id="listMovie" className="list-movie container space-y-5">
      <div className="list-movie-header pt-5 pb-10">
        <button
          className={` ${activeClassDC} mr-2`}
          onClick={renderPhimDangChieu}
        >
          Phim bom tấn
        </button>
        <button className={`${activeClassSC} `} onClick={renderPhimSapChieu}>
          Phim sắp chiếu
        </button>
      </div>
      <Slider {...settings}>
        {movieList.map((movie, index) => {
          return <CardMovie movie={movie} key={index} />;
        })}
      </Slider>
    </div>
  );
}
