import moment from "moment/moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function TabMovieChild({ movie }) {
  let renderMovie = () => {
    return movie.map((item, index) => {
      return (
        <div key={index} className="flex mb-5 space-x-5 w-100">
          <img
            className="w-24"
            style={{ height: 150, objectFit: "cover" }}
            src={item.hinhAnh}
            alt=""
          />
          <div className="h-32 ">
            <p className="cine-detail-movie-name">{item.tenPhim}</p>
            <div className="h-32 overflow-y-auto grid grid-cols-2">
              {item.lstLichChieuTheoPhim.length > 4
                ? item.lstLichChieuTheoPhim.slice(0, 4).map((lich, i) => {
                    console.log("lich: ", item.lstLichChieuTheoPhims);
                    return (
                      <NavLink
                        to={`/check-out/${lich.maLichChieu}`}
                        key={i}
                        className="border-2 p-2 rounded shadow bg-gray-200 my-2 mx-2 h-10 hover:scale-110 hover:cursor-pointer"
                      >
                        <span className="text-green-600">
                          {moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}
                        </span>{" "}
                        ~{" "}
                        <span className="text-red-500 text-lg font-medium">
                          {moment(lich.ngayChieuGioChieu).format("LT")}
                        </span>
                      </NavLink>
                    );
                  })
                : item.lstLichChieuTheoPhim.map((lich, i) => {
                    console.log("lich: ", item.lstLichChieuTheoPhims);
                    return (
                      <NavLink
                        to={`/check-out/${lich.maLichChieu}`}
                        key={i}
                        className="border-2 p-2 rounded shadow bg-gray-200 my-2 mx-2 h-10 hover:scale-105 hover:cursor-pointer"
                      >
                        <span className="text-green-600">
                          {moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}
                        </span>{" "}
                        ~{" "}
                        <span className="text-red-500 text-xl font-medium">
                          {moment(lich.ngayChieuGioChieu).format("LT")}
                        </span>
                      </NavLink>
                    );
                  })}
              {}
            </div>
          </div>
        </div>
      );
    });
  };
  return <div>{renderMovie()}</div>;
}
