import React, { useEffect, useState } from "react";
import "../../../../styles/pages/tabmovie.scss";
import { Tabs } from "antd";
import { movieServ } from "../../../Service/movieServ";
import TabMovieChild from "./TabMovieChild";
import { useDispatch } from "react-redux";
import {
  batSpinnerAction,
  tatSpinnerAction,
} from "../../../redux/action/spinnerAction";

export default function TabMovie() {
  // const items = [
  //   {
  //     key: '1',
  //     label: `Tab 1`,
  //     children: `Content of Tab Pane 1`,
  //   },]
  let dispatch = useDispatch();
  const [dataMovie, setDataMovie] = useState([]);
  // console.log('dataMovie: ', dataMovie);
  useEffect(() => {
    dispatch(batSpinnerAction());
    movieServ
      .getMovieByTheater()
      .then((res) => {
        dispatch(tatSpinnerAction());
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        dispatch(tatSpinnerAction());
        console.log(err);
      });
  }, []);

  return (
    <div id="rapPhim" className="cine container mb-5">
      {
        <Tabs
          style={{ height: 700 }}
          tabPosition="left"
          defaultActiveKey="1"
          items={dataMovie.map((item, index) => {
            return {
              key: item.maHeThongRap,
              label: (
                <img src={item.logo} alt="logo" className="cine-name w-16" />
              ),
              children: (
                <Tabs
                  style={{ height: 700 }}
                  tabPosition="left"
                  defaultActiveKey="1"
                  items={item.lstCumRap.map((cumRap) => {
                    return {
                      key: cumRap.diaChi,
                      label: (
                        <div className="cine-detail text-left w-80">
                          <p className=" text-md cine-detail-name ">
                            {cumRap.tenCumRap}
                          </p>
                          <p className="hover:overflow-scroll cine-address opacity-90">
                            {cumRap.diaChi}
                          </p>
                        </div>
                      ),
                      children: (
                        <div
                          className="cine-movie-detail overflow-y-auto text-center"
                          style={{ height: 700 }}
                        >
                          <TabMovieChild movie={cumRap.danhSachPhim} />
                        </div>
                      ),
                    };
                  })}
                />
              ),
            };
          })}
        />
      }
    </div>
  );
}
