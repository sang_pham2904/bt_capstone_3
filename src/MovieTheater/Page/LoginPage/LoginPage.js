import React from 'react'
import { Button, Checkbox, Form, Input, message } from 'antd';
import { userServ } from '../../Service/userService';
import { localServ } from '../../Service/localServ';
import { useDispatch } from 'react-redux';
import { LOGIN } from '../../redux/constant/userConstant';
import { userAction, userActionThunk } from '../../redux/action/userAction';
import { NavLink, useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import  login_ani  from '../../asset/login_ani.json'
export default function LoginPage() {
  let navigate = useNavigate()
  let dispatch = useDispatch()
  let onSuccess = (res) => {
    message.success('Đăng nhập thành công')
    localServ.set(res.data.content)
    navigate('/')
  }
  let onErr = (err) => {
    message.error(err)
  }
  const onFinish = (values) => {
    console.log('Success:', values);
    dispatch(userActionThunk(values,onSuccess,onErr))
    // userServ.postLogin(values)
    //     .then((res) => {
    //         console.log(res.data.content);
    //         localServ.set(res.data.content)
    //         message.success('Đăng nhập thành công')
    //         dispatch(userAction(res.data.content))
    //       })
    //       .catch((err) => {
    //        console.log(err);
    //        message.error('Đăng nhập thất bại')
    //       });
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className='flex justify-center items-center h-screen w-screen bg-blue-300'>
      <div className='bg-white w-8/12 pb-8 pt-12 flex items-center'>
        <div className='w-1/2'>
          <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Tài khoản"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
    
        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
    
    
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className='bg-blue-600' type="primary" htmlType="submit">
            Đăng nhập
          </Button>
          <br/>
          <NavLink className='mt-3 text-blue-600 inline-block' to={'/sign-up'}>Bạn chưa có tài khoản? Đăng ký</NavLink>
        </Form.Item>
      </Form>
        </div>
        <div className='w-1/2 flex justify-center'>
        <Lottie animationData={login_ani} loop={true} style={{width : 200}} />
        </div>
      </div>
    </div>
  )
}
