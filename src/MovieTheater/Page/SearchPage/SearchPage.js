import axios, { Axios } from 'axios';
import React, { useCallback, useEffect, useState } from 'react'
import { BASE_URL, configHeaders } from '../../Service/Config';
import { debounce } from 'lodash';

export default function SearchPage() {
    
    const [visible, setVisible] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [dropdownOptions, setDropdownOptions] = useState([]);
    console.log('dropdownOptions: ', dropdownOptions);
  
    function openDropdown() {
      setVisible(true);
    }
  
    function fetchDropdownOptions(key) {
      axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`,{headers : configHeaders()}).then((res) => {
            //   console.log(res.data.content);
              let indexPhimSearch = res.data.content.filter((item) => {
                  return item.tenPhim.toLowerCase().includes(key)
                })
                // console.log('indexPhimSearch: ', indexPhimSearch);
            //   console.log('tenPhim: ', tenPhim);
              setDropdownOptions(indexPhimSearch)

            })
            .catch((err) => {
             console.log(err);
            });;
    }

    // có debounce
    const debounceDropDown = useCallback(debounce((nextValue) => fetchDropdownOptions(nextValue), 1000), [])

    function handleInputOnchange(e) {
      const { value } = e.target; 
      setKeyword(value);

    //   không có debounce
    //   fetchDropdownOptions(value);

    // có debounce
    debounceDropDown(value)
    }


    function SearchResults() {
      const [data, setData] = useState([]);
      const [searchTerm, setSearchTerm] = useState('');
      const [filteredData, setFilteredData] = useState([]);
    
      useEffect(() => {
        async function fetchData() {
          const result = await axios.get('https://example.com/api/data');
          setData(result.data);
        }
        fetchData();
      }, []);
    
      useEffect(() => {
        function filterData() {
          const results = data.filter((item) =>
            item.name.toLowerCase().includes(searchTerm.toLowerCase())
          );
          setFilteredData(results);
        }
        filterData();
      }, [data, searchTerm]);
    }

  return (
    <div>
      <input value={keyword} placeholder='Enter string' onClick={openDropdown} onChange={handleInputOnchange} />
      <div className='grid grid-cols-3'>
        {
          visible ? dropdownOptions?.map((item,index) => {
           return <div key={index}>
            {item.tenPhim}
           </div>
          }) : null
        }
      </div>
    </div>
  )
}
