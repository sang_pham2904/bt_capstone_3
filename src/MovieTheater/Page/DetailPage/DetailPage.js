import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../Service/movieServ";
import moment from "moment/moment";
import { Progress, Space } from "antd";
import { Rate } from "antd";
import { Tabs } from "antd";
import Header from "../../Component/Header";

export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState([]);
  const [value, setValue] = useState();
  console.log("movie: ", movie);
  useEffect(() => {
    movieServ
      .getMovieDetail(params.id)
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
        let { danhGia } = res.data.content;
        if (danhGia >= 0 && danhGia <= 1) {
          setValue(0.5);
        } else if (danhGia > 1 && danhGia <= 2) {
          setValue(1);
        } else if (danhGia > 2 && danhGia <= 3) {
          setValue(1.5);
        } else if (danhGia > 3 && danhGia <= 4) {
          setValue(2);
        } else if (danhGia > 4 && danhGia <= 5) {
          setValue(2.5);
        } else if (danhGia > 5 && danhGia <= 6) {
          setValue(3);
        } else if (danhGia > 6 && danhGia <= 7) {
          setValue(3.5);
        } else if (danhGia > 7 && danhGia <= 8) {
          setValue(4);
        } else if (danhGia > 8 && danhGia <= 9) {
          setValue(4.5);
        } else if (danhGia > 9 && danhGia <= 10) {
          setValue(5);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  // flex container space-x-5 items-center justify-between
  const items = [
    {
      key: "1",
      label: `Lịch chiếu`,
      children: (
        <div className="container">
          <Tabs
            defaultActiveKey="1"
            tabPosition="left"
            items={movie.heThongRapChieu?.map((item) => {
              return {
                key: item.maHeThongRap,
                label: <img src={item.logo} alt="logo" className="w-16" />,
                children: item.cumRapChieu?.map((cumRap, index) => {
                  return (
                    <div key={index}>
                      <div className="">
                        <div>
                          <p className="text-2xl text-green-600">
                            {cumRap.tenCumRap}
                          </p>
                          <p>{cumRap.diaChi}</p>
                        </div>
                        <div className="grid grid-col-1 md:grid-cols-2 lg:grid-cols-3">
                          {cumRap.lichChieuPhim?.map((lich, i) => {
                            return (
                              <NavLink
                                key={i}
                                to={`/check-out/${lich.maLichChieu}`}
                                className="border-2 p-2 rounded shadow bg-gray-200 my-2 mx-2 h-10 hover:scale-105 hover:cursor-pointer w-52"
                              >
                                <span className="text-lg">
                                  {moment(lich.ngayChieuGioChieu).format(
                                    "DD-MM-YYYY"
                                  )}
                                </span>{" "}
                                ~
                                <span className="text-red-500 text-lg">
                                  {" "}
                                  {moment(lich.ngayChieuGioChieu).format("LT")}
                                </span>
                              </NavLink>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  );
                }),
              };
            })}
          />
        </div>
      ),
    },
    {
      key: "2",
      label: `Thông tin`,
      children: (
        <div className="flex justify-center">
          <div className="w-1/3 space-y-3 text-lg">
            <p>Ngày khởi chiếu : {moment(movie.ngayKhoiChieu).format("L")}</p>
            <p>Thể loại: Hành động</p>
            <p>Đạo diễn: Mc Larance</p>
            <p>Diễn viên: The Rock</p>
            <p>Định dạng: 2D</p>
            <p>Quốc gia sản xuất: Việt Nam</p>
          </div>
          <div className="w-2/3 text-lg">
            <p>{movie.moTa}</p>
          </div>
        </div>
      ),
    },
    {
      key: "3",
      label: `Đánh giá`,
      children: (
        <div className="flex flex-col items-center space-y-5">
          <Progress
            type="circle"
            percent={movie.danhGia * 10}
            status="normal"
            format={(percent) => `${percent / 10} Điểm`}
            size={150}
            strokeColor={{ "0%": "#108ee9", "100%": "#87d068" }}
          />

          <Rate onChange={setValue} allowHalf value={value} />
        </div>
      ),
    },
  ];

  return (
    <div className="container mt-20">
      <div className="grid grid-cols-1 lg:grid-cols-3 container">
        <div className="flex justify-center">
          <img className="w-60" src={movie.hinhAnh} alt="" />
        </div>
        <div className="text-center space-y-5 mt-3">
          <p className="text-red-400 text-xl">
            Khởi chiếu: {moment(movie.ngayKhoiChieu).format("L")}
          </p>
          <h2 className="text-3xl font-normal text-blue-500">
            {movie.tenPhim}
          </h2>
          <p className="text-cyan-600">{movie.moTa}</p>
        </div>
        <div className="flex flex-col items-center space-y-5">
          <Progress
            type="circle"
            percent={movie.danhGia * 10}
            status="normal"
            format={(percent) => `${percent / 10} Điểm`}
            size={150}
            strokeColor={{ "0%": "#108ee9", "100%": "#87d068" }}
          />

          <Rate onChange={setValue} allowHalf value={value} />
        </div>
      </div>
      <div className="container">
        <Tabs defaultActiveKey="1" items={items} centered />
      </div>
      {/* <div className="container">
    <Tabs defaultActiveKey="1" tabPosition="left" items={movie.heThongRapChieu?.map((item,index) => {
     return{
      key: item.maHeThongRap,
      label: <img src={item.logo} alt='logo' className='w-16'/>,
      children: `Content of Tab Pane 1`,
     }
    })}  />
    </div> */}
    </div>
  );
}
