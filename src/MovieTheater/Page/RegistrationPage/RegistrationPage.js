import React from "react";
import Lottie from "lottie-react";
import login_ani from "../../asset/login_ani.json";
import { Button, Form, Input, InputNumber, message, } from "antd";
import { useDispatch } from "react-redux";
import { registerServ } from "../../Service/RegisterServ";
import { postRegisterAction } from "../../redux/action/registerAction";
import { NavLink, useNavigate } from "react-router-dom";
import { localServ } from "../../Service/localServ";
import { batSpinnerAction, tatSpinnerAction } from "../../redux/action/spinnerAction";
import { userServ } from "../../Service/userService";
import { userAction } from "../../redux/action/userAction";


export default function RegistrationPage() {
    let dispatch = useDispatch()
    let navigate = useNavigate()
  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    dispatch(batSpinnerAction())
    registerServ.postRegister(values)
            .then((res) => {
                dispatch(tatSpinnerAction())
            console.log(res);
            dispatch(postRegisterAction(res.data.content))
            localServ.set(res.data.content)

                userServ.postLogin(res.data.content)
        .then((response) => {
            console.log(response.data.content);
            localServ.set(response.data.content)

            dispatch(userAction(response.data.content))
          })
          .catch((error) => {
           console.log(error);

          });

            navigate('/')
          })
          .catch((err) => {
            dispatch(tatSpinnerAction())
           console.log(err);
           message.error(err.response.data.content)
          });
  };

//   const maNhoms = 'GP00'

  return (
    <div className="flex justify-center items-center h-screen w-screen bg-blue-300">
      
      <div className="bg-white w-8/12 pb-8 pt-12 flex items-center">
        <div className="w-1/2">
          <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            // initialValues={{
            //   maNhom: 'GP00',
            //   prefix: "86",
            // }}
            style={{
              maxWidth: 600,
            }}
            scrollToFirstError
          >
            <Form.Item
              name="taiKhoan"
              label="Tài khoản"
              // tooltip="What do you want others to call you?"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền tài khoản!",
                  whitespace: false,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="matKhau"
              label="Mật khẩu"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
                { min: 6, message: "Mật khẩu phải từ 6 kí tự." },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="confirm"
              label="Xác nhận mật khẩu"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập lại mật khẩu vừa đặt!",
                },

                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("matKhau") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error(
                        "Mật khẩu không trùng khớp!"
                      )
                    );
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="hoTen"
              label="Họ Tên"
              // tooltip="What do you want others to call you?"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền tài khoản!",
                  whitespace: true,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "Định dạng Email chưa đúng!",
                },
                {
                  required: true,
                  message: "Vui lòng nhập Email!",
                },
              ]}
            >
              <Input />
            </Form.Item>

{/* 
                <Form.Item className="hidden"
                  name="maNhom" 
                  
                >
                  <Input />
                </Form.Item> */}
           

            <Form.Item
        name="soDt"
        label="Số điện thoại"
        rules={[
          {
            required: true,
            message: 'Vui lòng điền vào số điện thoại của bạn!',
            type: 'number'
          },
        //   { min: 6, message: "Số điện thoại." },



        ]}
      >
        <InputNumber
          
          style={{
            width: '100%',
          }}
        />
      </Form.Item>

            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" className="bg-blue-500">
                Đăng ký
              </Button>
              <br/>
              <NavLink className='inline-block mt-3 text-blue-600'>Bạn đã có tài khoản? Đăng nhập</NavLink>
            </Form.Item>
          </Form>
        </div>
        <div className="w-1/2 flex justify-center">
          <Lottie
            animationData={login_ani}
            loop={true}
            style={{ width: 200 }}
          />
        </div>
      </div>
    </div>
  );
}
