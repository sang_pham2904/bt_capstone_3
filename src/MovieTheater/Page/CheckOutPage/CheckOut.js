import React, { Fragment, useEffect, useState } from "react";
import { localServ } from "../../Service/localServ";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import style from "./screen.css";
import { datVeServ } from "../../Service/datVeServ";
import {
  chonGheAction,
  datGheAction,
  datVeAction,
} from "../../redux/action/datVeAction";
import {
  batSpinnerAction,
  tatSpinnerAction,
} from "../../redux/action/spinnerAction";
import _ from "lodash";
import { UserOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { Button, Modal } from "antd";
// import { connection } from "../../..";

export default function CheckOut() {
  const { userInfo } = useSelector((state) => state.userReducer);
  // console.log("userInfo: ", userInfo);
  const { chiTietPhongVe, danhSachGheDaChon, danhSachGheKhachDat } =
    useSelector((state) => state.datVeReducer);
  // console.log("danhSachGheDaDat: ", danhSachGheDaChon);
  let { thongTinPhim, danhSachGhe } = chiTietPhongVe;
  // console.log("chiTietPhongVe: ", chiTietPhongVe);
  // const userInfo = localServ.get()
  let navigate = useNavigate();
  let params = useParams();
  let dispatch = useDispatch();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpenSec, setIsModalOpenSec] = useState(false);
  const [isModalOpenSauThanhToan, setIsModalOpenSauThanhToan] = useState(false);
  const [isModalOpenTimeOut, setIsModalOpenTimeOut] = useState(false);

  const [timeState, setTime] = useState(300);

  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleOkSec = () => {
    setIsModalOpenSec(false);
  };
  const handleCancelSec = () => {
    setIsModalOpenSec(false);
  };

  const handleOkSauThanhToan = () => {
    setIsModalOpenSauThanhToan(false);
  };
  const handleCancelSauThanhToan = () => {
    setIsModalOpenSauThanhToan(false);
  };

  const handleOkTimeOut = () => {
    setIsModalOpenTimeOut(false);
  };
  const handleCancelTimeOut = () => {
    setIsModalOpenTimeOut(false);

    window.location.reload();
  };

  useEffect(() => {
    if (!userInfo) {
      return navigate("/login");
    }
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    dispatch(batSpinnerAction());

    datVeServ
      .getDatVe(params.id)
      .then((res) => {
        dispatch(tatSpinnerAction());
        dispatch(datVeAction(res.data.content));
      })
      .catch((err) => {
        dispatch(tatSpinnerAction());
        console.log(err);
      });
  }, []);

  let renderGhe = () => {
    return danhSachGhe.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      let classGheKhachDat = "";

      let indexGheDangDat = danhSachGheDaChon.findIndex((gheDangDat) => {
        return gheDangDat.maGhe === ghe.maGhe;
      });
      if (indexGheDangDat != -1) {
        classGheDaDat = "gheDangDat";
      } else if (userInfo?.taiKhoan == ghe.taiKhoanNguoiDat) {
        classGheDaDat = "gheUserDat";
      }

      return (
        <Fragment key={index}>
          <button
            disabled={ghe.daDat || classGheKhachDat !== ""}
            onClick={() => {
              // const action = datGheAction(ghe,params.id,)
              dispatch(chonGheAction(ghe));
            }}
            className={`ghe ${classGheVip} ${classGheDaDat} ${classGheKhachDat}`}
          >
            {userInfo?.taiKhoan == ghe.taiKhoanNguoiDat ? (
              <UserOutlined />
            ) : (
              ghe.stt
            )}
          </button>
          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };

  let dataDatVe = {
    maLichChieu: params.id,
    danhSachVe: danhSachGheDaChon,
  };
  let tokenUser = userInfo?.accessToken;

  let handleDatVe = () => {
    if (userInfo == null) {
      navigate("/login");
    } else {
      if (danhSachGheDaChon.length == 0) {
        setIsModalOpen(true);
      } else {
        setIsModalOpenSec(true);
      }
    }
  };

  let datVeVaThanhToan = () => {
    datVeServ
      .postDatVe(dataDatVe, tokenUser)
      .then((res) => {
        console.log(res);
        setIsModalOpenSauThanhToan(true);
        // window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    let time = 300;
    let myCountDown = setInterval(() => {
      // console.log(Math.floor(time / 60), ":", time % 60);
      time--;
      setTime(time);
      if (time <= 0) {
        clearInterval(myCountDown);
        setIsModalOpenTimeOut(true);
      }
    }, 1000);
    return () => {
      clearInterval(myCountDown);
    };
  }, []);

  return (
    <div className="min-h-screen mt-20">
      <div className="grid grid-cols-12 container">
        <div className="col-span-9 overflow-x-auto">
          <div style={{ width: "1100px" }}>
            <div className="manHinh text-center text-2xl ml-3 mb-20">
              Màn hình
            </div>
            <div>{renderGhe()}</div>
            <div className="mt-14 flex justify-center">
              <table className="divide-y divide-gray-200 w-full">
                <thead>
                  <tr>
                    <th className="w-1/6">Ghế thường</th>
                    <th className="w-1/6">Ghế VIP</th>
                    <th className="w-1/6">Ghế đang chọn</th>
                    <th className="w-1/6">Ghế đã được đặt</th>
                    <th className="w-1/6">Ghế bạn đã thanh toán</th>
                    {/* <th className="w-1/6">Ghế khách đang đặt</th> */}
                  </tr>
                </thead>
                <tbody>
                  <tr className="text-center">
                    <td>
                      <button className="ghe"></button>
                    </td>
                    <td>
                      <button className="ghe gheVip"></button>
                    </td>
                    <td>
                      <button className="ghe gheDangDat"></button>
                    </td>
                    <td>
                      <button className="ghe gheDaDat"></button>
                    </td>
                    <td>
                      <button className="ghe gheUserDat">
                        <UserOutlined />
                      </button>
                    </td>
                    {/* <td>
                      <button className="ghe gheKhachDat"></button>
                    </td> */}
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="col-span-1"></div>
        <div className="col-span-2">
          <div className="text-center">
            <p>
              <span> {`${Math.floor(timeState / 60)}`} : </span>
              <span> {`0${timeState % 60}`.slice(-2)}</span>
            </p>
          </div>
          <h3 className="text-center text-green-400 text-2xl">
            {danhSachGheDaChon
              .reduce((tong, item, index) => {
                return (tong += item.giaVe);
              }, 0)
              .toLocaleString()}{" "}
            đ
          </h3>
          <hr />
          <h3 className="text-xl">{thongTinPhim.tenPhim}</h3>
          <br />
          <p>
            Rạp phim :{" "}
            <span className="text-green-500">{thongTinPhim.tenCumRap}</span>
          </p>
          <br />
          <p>Địa chỉ : {thongTinPhim.diaChi}</p>
          <br />
          <p>
            Ngày giờ chiếu :{" "}
            <span className="text-green-500">
              {thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}
            </span>
          </p>
          <br />
          <p className="text-green-500">{thongTinPhim.tenRap}</p>
          <hr />
          <br />
          <div className="grid grid-cols-2">
            <div className="w-4/5">
              <span className="text-red-400">
                Ghế:
                {_.sortBy(danhSachGheDaChon, ["stt"]).map((gheDD, index) => {
                  return <span key={index}> {gheDD.stt}</span>;
                })}
              </span>
            </div>
            <div className="text-right pr-10">
              <span className="text-green-400">
                {danhSachGheDaChon
                  .reduce((tong, item, index) => {
                    return (tong += item.giaVe);
                  }, 0)
                  .toLocaleString()}{" "}
                đ
              </span>
            </div>
          </div>
          <br />
          <hr />
          <div>
            <i>Email</i> : {userInfo?.email}
          </div>
          <br />
          <div>
            <i>Số điện thoại</i> : {userInfo?.soDT}
          </div>
          <div className="mt-10">
            <div
              className="bg-cyan-500 text-white w-full text-center py-3 font-bold text-xl rounded cursor-pointer"
              onClick={handleDatVe}
            >
              Đặt vé
            </div>
          </div>
        </div>
      </div>

      <Modal
        title=""
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={
          <Button
            className="bg-cyan-500 text-white"
            key="back"
            onClick={handleCancel}
          >
            OK
          </Button>
        }
      >
        <div className="text-center space-y-5">
          <CloseCircleOutlined style={{ fontSize: "80px", color: "red" }} />
          <p className="text-3xl">Bạn chưa chọn ghế</p>
          <p className="text-2xl text-green-500">Vui lòng chọn ghế</p>
        </div>
      </Modal>

      <Modal
        title="Thông tin đặt vé"
        open={isModalOpenSec}
        onOk={handleOkSec}
        onCancel={handleCancelSec}
        footer={""}
      >
        <div className="space-y-10">
          <h3 className="text-3xl">{thongTinPhim.tenPhim}</h3>
          <p className="text-2xl">
            Rạp phim :{" "}
            <span className="text-green-500">{thongTinPhim.tenCumRap}</span>
          </p>
          <p className="text-2xl">
            Ngày giờ chiếu :{" "}
            <span className="text-green-500">
              {thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}
            </span>
          </p>

          <div className="w-4/5 text-2xl">
            <span className="text-red-400">
              Ghế:
              {_.sortBy(danhSachGheDaChon, ["stt"]).map((gheDD, index) => {
                return <span key={index}> {gheDD.stt}</span>;
              })}
            </span>
          </div>
          <div className="text-center">
            <span className="text-green-400 text-3xl">
              Tổng :{" "}
              {danhSachGheDaChon
                .reduce((tong, item, index) => {
                  return (tong += item.giaVe);
                }, 0)
                .toLocaleString()}{" "}
              đ
            </span>
          </div>
          <div className="text-center mt-5">
            <button
              className="text-white bg-cyan-500 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-cyan-500 dark:hover:bg-cyan-600 focus:outline-none dark:focus:ring-cyan-800"
              onClick={datVeVaThanhToan}
            >
              Xác nhận đặt vé và thanh toán
            </button>
          </div>
        </div>
      </Modal>

      <Modal
        title=""
        open={isModalOpenSauThanhToan}
        onOk={handleOkSauThanhToan}
        onCancel={handleCancelSauThanhToan}
        footer={""}
      >
        <p className="text-center text-3xl">Đặt vé thành công</p>

        <div className="text-center mt-5">
          <button
            className="text-white bg-cyan-500 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-cyan-500 dark:hover:bg-cyan-600 focus:outline-none dark:focus:ring-cyan-800"
            onClick={() => {
              window.location.reload();
            }}
          >
            Tiếp tục đặt thêm vé
          </button>
          <button
            className="text-white bg-cyan-500 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-cyan-500 dark:hover:bg-cyan-600 focus:outline-none dark:focus:ring-cyan-800"
            onClick={() => {
              window.location.href = '/'
            }}
          >
            Quay lại trang chủ
          </button>
        </div>
      </Modal>

      <Modal
        title=""
        open={isModalOpenTimeOut}
        onOk={handleOkTimeOut}
        onCancel={handleCancelTimeOut}
        footer={""}
      >
        <p className="text-center text-3xl">
          Tiếc quá hết thời gian giữ chỗ rồi
        </p>

        <div className="text-center mt-5">
          <button
            className="text-white bg-cyan-500 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-cyan-500 dark:hover:bg-cyan-600 focus:outline-none dark:focus:ring-cyan-800"
            onClick={() => {
              window.location.reload();
            }}
          >
            Tiếp tục đặt lại vé
          </button>
          <button
            className="text-white bg-cyan-500 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-cyan-500 dark:hover:bg-cyan-600 focus:outline-none dark:focus:ring-cyan-800"
            onClick={() => {
              navigate("/");
            }}
          >
            Quay lại trang chủ
          </button>
        </div>
      </Modal>
    </div>
  );
}
