import React from "react";
import "../../../styles/pages/footer.scss";

export default function Footer() {
  return (
    <div>
      <footer
        id="section-footer"
        data-section-id="footer"
        data-section-type="footer"
        className="footer footer-center"
        role="contentinfo"
      >
        <div className="container ">
          <div className="footer-inner flex justify-center items-center ">
            <div className="footer-block footer-block-link">
              <h2 className="footer-title heading">CINE & CHILL</h2>
              <ul className="linklist">
                <li className="linklist-item">
                  <a href="#" className="link link-primary">
                    Chính sách bảo mật
                  </a>
                </li>
                <li className="linklist-item">
                  <a href="#" className="link link-primary">
                    Thông tin tài khoản
                  </a>
                </li>
                <li className="linklist-item">
                  <a href="#" className="link link-primary">
                    Điều khoản sử dụng
                  </a>
                </li>

                <li className="linklist-item">
                  <a href="#" className="link link-primary">
                    Khuyến mãi và Sự kiện
                  </a>
                </li>
                <li className="linklist-item">
                  <a href="#" className="link link-primary">
                    Liên hệ
                  </a>
                </li>
                <li className="linklist-item">
                  <a href="#" className="link link-primary">
                    Về chúng tôi
                  </a>
                </li>
              </ul>
            </div>
            <div className="footer-block footer-block-newsletter">
              <h2 className="footer-title Heading ">Newsletter</h2>
              <div className="footer-content">
                <p>
                  Đăng ký thành viên để được hưởng những ưu đãi tuyệt vời nhất
                </p>
              </div>
              <form
                method="post"
                action="/contact#footer-newsletter"
                id="footer-newsletter"
                acceptCharset="UTF-8"
                className="footer-newsletter Form"
              >
                <input type="hidden" name="form_type" defaultValue="customer" />
                <input type="hidden" name="utf8" defaultValue="✓" />
                <input
                  type="hidden"
                  name="contact[tags]"
                  defaultValue="newsletter"
                />
                <input
                  type="email"
                  name="contact[email]"
                  className="form-input"
                  aria-label="Enter your email address"
                  placeholder="Enter your email address"
                  required
                />
                <button type="submit" className="form-submit">
                  Subscribe
                </button>
              </form>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
