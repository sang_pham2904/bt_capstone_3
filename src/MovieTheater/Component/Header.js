import React, { useEffect } from "react";
import "../../styles/pages/header.scss";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { localServ } from "../Service/localServ";
import ListMovie from "../Page/HomePage/ListMovie";
import { userServ } from "../Service/userService";
import {
  batSpinnerAction,
  tatSpinnerAction,
} from "../redux/action/spinnerAction";
import { Button, Modal } from "antd";
import { useState } from "react";
import moment from "moment/moment";
import _ from "lodash";
import { HashLink } from "react-router-hash-link";

export default function Header(props) {
  //set header position
  const { isFixed } = props;

  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  // console.log("userInfo: ", userInfo);
  let btnCss = "px-5 py-2";
  let navigate = useNavigate();
  let dispatch = useDispatch();

  // console.log("thongTinDatVeUser: ", thongTinDatVeUser);
  let logIn = () => {
    navigate("/login");
  };
  let logOut = () => {
    localServ.remove();
    window.location.reload();
  };
  let signUp = () => {
    navigate("/sign-up");
  };
  // console.log(userInfo);
  // console.log("userInfo: ", userInfo);

  //  let tokenUser = userInfo.accessToken

  // console.log('tokenUser: ', tokenUser);
  const [thongTinDatVeUser, setThongTinDatVeUser] = useState({});
  const [isModalOpenUser, setIsModalOpenUser] = useState(false);

  const handleOkUser = () => {
    setIsModalOpenUser(false);
  };
  const handleCancelUser = () => {
    setIsModalOpenUser(false);
  };

  //lich su dat ve
  let renderLichSuDatVe = () => {
    return thongTinDatVeUser.thongTinDatVe?.map((item, index) => {
      return (
        <div key={index} className="mb-5">
          <p>
            Ngày đặt : {moment(item.ngayDat).format("L")} |{" "}
            {moment(item.ngayDat).format("LT")}
          </p>
          <p>Tên phim : {item.tenPhim}</p>
          <p>
            Thời lượng : {item.thoiLuongPhim} phút, Giá vé :{" "}
            {item.giaVe.toLocaleString()} VNĐ
          </p>
          <p>{item.danhSachGhe[0].tenHeThongRap}</p>
          <p>{item.danhSachGhe[0].tenRap}</p>
          <p>
            Số ghế :{" "}
            {_.sortBy(item.danhSachGhe, ["tenGhe"]).map((ghe, i) => {
              return <span key={i}>{ghe.tenGhe} </span>;
            })}
          </p>
        </div>
      );
    });
  };

  //truy xuat thong tin User
  let layThongTinUser = () => {
    dispatch(batSpinnerAction());
    setIsModalOpenUser(true);
    userServ
      .postThongTinUser("", userInfo?.accessToken)
      .then((res) => {
        dispatch(tatSpinnerAction());
        // console.log(res.data.content);
        setThongTinDatVeUser(res.data.content);
      })
      .catch((err) => {
        dispatch(tatSpinnerAction());
        console.log(err);
      });
  };

  //login-sign up button
  let renderHeader = () => {
    if (userInfo) {
      return (
        <>
          <span className="cursor-pointer" onClick={layThongTinUser}>
            {userInfo.hoTen}
          </span>
          <button onClick={logOut} className={btnCss}>
            {" "}
            Đăng xuất
          </button>

          {/* <button onClick={logIn} className={btnCss}>Đăng nhập</button>
        <button onClick={signUp} className={btnCss}>Đăng ký</button> */}
        </>
      );
    } else {
      return (
        <>
          <button onClick={logIn} className={btnCss}>
            Đăng nhập
          </button>
          <button onClick={signUp} className={btnCss}>
            Đăng ký
          </button>

          {/* <span className='cursor-pointer' onClick={layThongTinUser}>{userInfo.hoTen}</span>
    <button onClick={logOut} className={btnCss}> Đăng xuất</button> */}
        </>
      );
    }
  };

  //set background to different color when scroll
  const [scrolled, setScrolled] = useState(false);
  useEffect(() => {
    const hanldeScroll = () => {
      setScrolled(window.pageYOffset > 30);
    };
    window.addEventListener("scroll", hanldeScroll);

    return () => {
      window.removeEventListener("scroll", hanldeScroll);
    };
  }, []);

  // let params = useParams();
  // console.log(params);

  return (
    <header
      className={`header ${scrolled ? "scrolled" : ""} ${
        isFixed ? "header-fixed" : "header"
      } shadow w-full z-50`}
    >
      <div className="container h-20 mx-auto flex justify-between items-center">
        <NavLink to={"/"}>
          <span className="header-logo inline-block cursor-pointer">
            CINE & CHILL
          </span>
        </NavLink>
        <div className="header-login space-x-4">{renderHeader()}</div>
      </div>

      <Modal
        title="Lịch sử đặt vé"
        open={isModalOpenUser}
        onOk={handleOkUser}
        onCancel={handleCancelUser}
        width={1000}
        footer={""}
      >
        <div className="grid grid-cols-2">{renderLichSuDatVe()}</div>
      </Modal>
    </header>
  );
}
