import React from 'react'
import { useSelector } from 'react-redux';
import RingLoader from "react-spinners/RingLoader";
export default function Spinner() {
   let {isLoading} = useSelector((state) => {
    return state.spinnerReducer
   })
  return (
    <div>
        {isLoading && <div className='fixed top-0 left-0 h-screen w-screen z-50 flex items-center  justify-center' style={{backgroundColor : 'rgba(0, 0, 0,0.7)'}}>
            <RingLoader color="#0077b6" size={200}/>
        </div>}
    </div>
  )
}
